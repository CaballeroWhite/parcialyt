<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/','youtube');

Auth::routes();

Route::get('youtube', 'BiodataController@index')->name('home');
route::resource('biodata','BiodataController');